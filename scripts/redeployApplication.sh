#!/bin/bash
## TODO - check exit codes!!

cd $TOP/expenditures

# compile
./gradlew clean && ./gradlew assemble

# IntelliJ
./gradlew cleanIdea && ./gradlew idea

# copy WAR file to Tomcat
cp build/libs/expenditures.war /usr/local/Cellar/tomcat/9.0.10/libexec/webapps/expenditures.war

# restart server
sh /usr/local/Cellar/tomcat/9.0.10/libexec/bin/catalina.sh stop
sleep 2
sh /usr/local/Cellar/tomcat/9.0.10/libexec/bin/setenv.sh
sh /usr/local/Cellar/tomcat/9.0.10/libexec/bin/catalina.sh start


# //FOR DEBUG
# emacs /usr/local/Cellar/tomcat/9.0.10/libexec/bin/setenv.sh
# //paste: export CATALINA_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8080"
# chmod +x /usr/local/Cellar/tomcat/9.0.10/libexec/bin/setenv.sh


# copy WAR file to Tomcat

# //run server
# sh /usr/local/Cellar/tomcat/9.0.10/libexec/bin/startup.sh