CREATE DATABASE finances;
\c finances;
-- CREATE TYPE expenditure_category AS ENUM ('Groceries',
--                                             'Restaurants',
--                                             'Travel',
--                                             'Vacation',
--                                             'Gifts',
--                                             'Miscellaneous',
--                                             'Personal Expenditures',
--                                             'Pharmacy & Home Supplies',
--                                             'Newspapers & Publications',
--                                             'Entertainment',
--                                             'Health & Fitness',
--                                             'Video Games',
--                                             'Housing & Rent',
--                                             'Clothing',
--                                             'Charity',
--                                             'Investments',
--                                             'Cash Expenditures',
--                                             'Education',
--                                             'Insurance',
--                                             'Medical Expenses',
--                                             'Internet & Utilities',
--                                             'Unknown');

create table t_categories (
    id serial primary key not null,
    name text unique not null check(length(name) > 0),
    is_custom boolean not null
);

insert into t_categories (id, name, is_custom) values 
    (default, 'Groceries', FALSE),
    (default, 'Restaurants', FALSE),
    (default, 'Travel', FALSE),
    (default, 'Vacation', FALSE),
    (default, 'Gifts', FALSE),
    (default, 'Miscellaneous', FALSE),
    (default, 'Personal Expenditures', FALSE),
    (default, 'Pharmacy & Home Supplies', FALSE),
    (default, 'Newspapers & Publications', FALSE),
    (default, 'Entertainment', FALSE),
    (default, 'Health & Fitness', FALSE),
    (default, 'Video Games', FALSE),
    (default, 'Housing & Rent', FALSE),
    (default, 'Clothing', FALSE),
    (default, 'Charity', FALSE),
    (default, 'Investments', FALSE),
    (default, 'Cash Expenditures', FALSE),
    (default, 'Education', FALSE),
    (default, 'Insurance', FALSE),
    (default, 'Medical Expenses', FALSE),
    (default, 'Internet & Utilities', FALSE),
    (default, 'Unknown', FALSE),
    (default, 'Beauty', FALSE),
    (default, 'Kids', FALSE),
    (default, 'Dating', FALSE),
    (default, 'Work', FALSE),
    (default, 'Art', FALSE);


/* Entries are of the form
id (integer)    ds (date)          merchant (text)                      category (numeric)     amount (numeric)
0           8/20/16             WHOLEFDS RVR 101 340 RI CAMBRIDGE   Groceries                           15.45
*/
CREATE TABLE t_expenditures (
    id          SERIAL                  PRIMARY KEY NOT NULL,
    ds          DATE                     NOT NULL,
    merchant    TEXT                   NOT NULL,
    category    smallint                NOT NULL references t_categories(id),
    amount      NUMERIC                 NOT NULL check (amount > 0),
    file_id     smallint                not null references t_parsed_files(id)
);

create table t_expenditures_backup(
    like t_expenditures including all
);

-- Expenditures by account type
create table t_account_type (
    expenditure_id integer primary key references t_expenditures (id) on delete cascade,
    type smallint check (type >= 0)
);

-- Merchants by category
create table t_merchants_by_category (
    merchant text primary key not null,
    category integer references t_categories (id)
);

create table t_merchants_by_category_backup (
    like t_merchants_by_category including all
);

-- Merchants to ignore
create table t_merchants_to_ignore (
    merchant text primary key not null check (length(merchant) > 0)
);

-- Parsed files
create table t_parsed_files (
    id serial primary key not null,
    file_name text not null,
    ds date not null
);

create table t_parsed_files_backup (
    like t_parsed_files including all
);






