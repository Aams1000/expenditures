package com.expenditures.parsers;

import com.expenditures.categories.CategoryLookup;
import com.expenditures.categories.ExpenditureCategory;
import com.expenditures.db.DBExpenditure;
import com.expenditures.parsers.db.ExpendituresUpdates;
import com.expenditures.parsers.db.PreviouslyParsedFiles;
import com.expenditures.merchants.db.MerchantsByCategory;
import com.expenditures.servlet.Logging;
import com.expenditures.util.StringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.collect.ImmutableList;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import javax.annotation.Nonnull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.io.*;
import java.util.*;

/**
 * Servlet to field parsing requests.
 *
 * Created by Andrew on 1.6.19.
 */

//V1 : KEEP TABLE OF ABSOLUTE FILE PATH TO DATE LAST UPDATED
//     ASSUME UNCHANGED IF DATE LAST UPDATED IS UNCHANGED
//1. Establish file is valid and does not match name and last modified date of entry in parsed files table
//2. Read in map of known merchants to entries
//3. Parse files, returning categorizable and uncategorizable entries
//4. Copy current table to backup table (overwriting previous backup)
//4. Insert known files into current table, store uncategorizable entries in uncategorizable table
//5.a Ask user to optionally verify inserted rows, give option to change category and flag whether all present/future
//    charges from the merchant should be changed to this)
//5.b Ask user to map merchants to categories (update tables accordingly)
//6. Ask user to map remaining entries to categories (insert into current table and update uncategorizable table accordingly)

@Path("/parse")
public class ParsingServlet {

    private static final String CSV_SUFFIX = ".csv";

    @GET
    public String actualTest() {
        final File file = new File("/Users/Andrew/FinancialData/2017/AmericanExpress/2018.csv");
        return realPost(file, CategoryLookup.INSTANCE);
    }
    @POST
    public String testPost() {
        return "POST method working";
    }

    public String realPost(@Nonnull final File file, @Nonnull final CategoryLookup categoryLookup) {
        final ParsingResult parsingResult = parseInitialFile(file, categoryLookup);
        final ImmutableList<ParsedExpenditure> allEntries = new ImmutableList.Builder<ParsedExpenditure>().addAll(parsingResult.getEntries())
                                                                                                          .build();
        final List<DBExpenditure> insertedExpenditures = ExpendituresUpdates.backupAndUpdateTable(parsingResult.getParsedFiles());
        final ObjectWriter mapper = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonString = null;
        try {
            jsonString = mapper.writeValueAsString(new ArrayList<>(allEntries));
        }
        catch (final JsonProcessingException e) {
            jsonString =  "Failed to convert object to JSON: " + e.getMessage();
        }
        return jsonString;
    }

    private static ParsingResult parseInitialFile(@Nonnull File file, @Nonnull final CategoryLookup categoryLookup) {
        final ParsingResult.Builder builder = new ParsingResult.Builder();
        final Map<String, Long> previouslyParsedFiles = PreviouslyParsedFiles.queryFileNamesAndDateLastModified();
        final Map<String, ExpenditureCategory> merchantsByCategory = MerchantsByCategory.queryMerchantsByCategory();
        recursiveParse(file, builder, merchantsByCategory, previouslyParsedFiles, categoryLookup);
        return builder.build();
    }

    private static void recursiveParse(@Nonnull final File file,
                                       @Nonnull final ParsingResult.Builder builder,
                                       @Nonnull final Map<String, ExpenditureCategory> merchantsByCategory,
                                       @Nonnull final Map<String, Long> previouslyParsedFiles,
                                       @Nonnull final CategoryLookup categoryLookup) {
        if (file.isDirectory()) {
            final File[] children = file.listFiles();
            if (children != null) {
                for (final File child : children) {
                    recursiveParse(child, builder, merchantsByCategory, previouslyParsedFiles, categoryLookup);
                }
            }
        }
        else if (isCsv(file)) {
            if (!hasFileAlreadyBeenParsed(file, previouslyParsedFiles)) {
                try {
                    final ParsedFile parsedFile = parseFile(file, merchantsByCategory, categoryLookup);
                    builder.addParsedFile(parsedFile);
                }
                catch (final UnexpectedFormatException | UnknownFormatException | IOException e) {
                    builder.addFailedFileWithReason(new FailedFileWithReason(file.getAbsolutePath(), e.toString()));
                }
            }
            else {
                builder.addSkippedFile(file.getAbsolutePath());
            }
        }
    }

    private static boolean hasFileAlreadyBeenParsed(@Nonnull final File file,
                                                    @Nonnull final Map<String, Long> previouslyParsedFiles) {
        return file.lastModified() == previouslyParsedFiles.getOrDefault(file.getAbsolutePath(), -1L);
    }

    private static boolean isCsv(@Nonnull final File file) {
        return file.getName().endsWith(CSV_SUFFIX);
    }

    @Nonnull
    private static ParsedFile parseFile(@Nonnull final File file,
                                        @Nonnull final Map<String, ExpenditureCategory> merchantsByCategory,
                                        @Nonnull final CategoryLookup categoryLookup) throws UnexpectedFormatException, UnknownFormatException, IOException {
        //assuming file has a header
        try (final BufferedReader reader = new BufferedReader(new FileReader(file))) {
            final String firstNonHeaderLine = getFirstNonHeaderLineFromReader(reader);
            if (!StringUtils.isBlank(firstNonHeaderLine)) {
                final Iterable<CSVRecord> entries = CSVFormat.RFC4180.withFirstRecordAsHeader().withSkipHeaderRecord().parse(reader);
                if (AmexParser.isCorrectFormat(firstNonHeaderLine)) {
                    return AmexParser.parse(entries, file.getAbsolutePath(), merchantsByCategory, categoryLookup);
                }
                else {
                    Logging.PARSER.error("Unable to recognize format of file " + file.getAbsolutePath());
                    throw new UnknownFormatException();
                }
            }
            else {
                Logging.PARSER.error("File " + file.getAbsolutePath() + " is empty after header, cannot parse.");
                throw new UnknownFormatException();
            }
        }
        catch (final IOException e) {
            Logging.PARSER.error("Failed to parse file " + file.getAbsolutePath() + " due to IOException: ", e);
            throw e;
        }
    }

    @Nonnull
    private static String getFirstNonHeaderLineFromReader(@Nonnull final BufferedReader reader) throws IOException {
        //remove header
        reader.readLine();

        String firstNonHeaderLine = "";
        String currLine = null;
        while (firstNonHeaderLine.length() == 0 && (currLine = reader.readLine()) != null) {
            firstNonHeaderLine = currLine;
        }
        //sets reader back to first line. This is kind of hacky, but for the moment it lets me avoid reading the file in twice
        reader.mark(0);
        reader.reset();

        return firstNonHeaderLine;
    }
}
