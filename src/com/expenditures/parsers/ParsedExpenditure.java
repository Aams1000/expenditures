package com.expenditures.parsers;

import javax.annotation.Nonnull;
import java.time.LocalDate;

/**
 * Same as DBExpenditure without EXPENDITURE_ID field (and category can be absent). Don't really like the copy/pasting... FIXME
 *
 */
public class ParsedExpenditure {

    private final LocalDate transactionDate;
    private final String merchant;
    private final double amount;
    private final int categoryId;

    public ParsedExpenditure(@Nonnull final LocalDate transactionDate,
                             @Nonnull final String merchant,
                             final double amount,
                             final int categoryId) {
        this.transactionDate = transactionDate;
        this.merchant = merchant;
        this.amount = amount;
        this.categoryId = categoryId;
    }

    @Nonnull
    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    @Nonnull
    public String getMerchant() {
        return merchant;
    }

    public double getAmount() {
        return amount;
    }

    public int getCategoryId() {
        return categoryId;
    }
}
