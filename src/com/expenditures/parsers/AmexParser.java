package com.expenditures.parsers;

import com.expenditures.categories.CategoryLookup;
import com.expenditures.categories.ExpenditureCategory;
import com.expenditures.servlet.Logging;
import com.expenditures.util.StringUtils;
import com.google.common.collect.ImmutableList;
import org.apache.commons.csv.CSVRecord;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class AmexParser {

    private static final Pattern LINE_FORMAT = Pattern.compile("\\d{4}[\\-]\\d{2}[\\-]\\d{2}[,]\\d{18}");
    private static final Pattern DATE_PATTERN = Pattern.compile("\\d{4}[\\-]\\d{2}[\\-]\\d{2}");

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");


    private enum CSVIndex {
        DATE(0),
        MERCHANT(2),
        AMOUNT(5);

        private final int index;

        CSVIndex(final int index) {
            this.index = index;
        }

        int getIndex() {
            return index;
        }
    }

    private AmexParser(){}

    static boolean isCorrectFormat(@Nonnull final String line) {
        return LINE_FORMAT.matcher(line).find();
    }

    @Nonnull
    static ParsedFile parse(@Nonnull final Iterable<CSVRecord> entries,
                            @Nonnull final String fileName,
                            @Nonnull final Map<String, ExpenditureCategory> merchantsByCategory,
                            @Nonnull final CategoryLookup categoryLookup) throws UnknownFormatException {
        final ImmutableList.Builder<ParsedExpenditure> builder = new ImmutableList.Builder<>();
        for (final CSVRecord entry : entries) {
            parseEntry(entry, fileName, merchantsByCategory, categoryLookup).ifPresent(builder::add);
        }
        return new ParsedFile(builder.build(), ParsedFile.AccountType.AMEX);
    }

    @Nonnull
    private static Optional<ParsedExpenditure> parseEntry(@Nonnull final CSVRecord entry,
                                                          final String fileName,
                                                          @Nonnull final Map<String, ExpenditureCategory> merchantsByCategory,
                                                          @Nonnull final CategoryLookup categoryLookup) {
        Optional<ParsedExpenditure> parsedExpenditure = Optional.empty();
        try {
            final double amount = Double.parseDouble(entry.get(CSVIndex.AMOUNT.getIndex()));
            final String merchant = entry.get(CSVIndex.MERCHANT.getIndex());
            final Matcher dateMatcher = DATE_PATTERN.matcher(entry.get(CSVIndex.DATE.getIndex()));
            final String dateString = dateMatcher.find() ? dateMatcher.group(0) : "";
            final LocalDate date = parseDateFromValue(dateString, DATE_TIME_FORMATTER);
            final int categoryId = merchantsByCategory.containsKey(merchant)
                    ? merchantsByCategory.get(merchant).getId()
                    : categoryLookup.getUnknownCategory().getId();
            if (amount > 0 && !StringUtils.isBlank(merchant)) {
                parsedExpenditure = Optional.of(new ParsedExpenditure(date, merchant, amount, categoryId));
            }
            else {
                Logging.PARSER.info("Skipping entry : " + entry.toString());
            }
        }
        catch (final ArrayIndexOutOfBoundsException | NumberFormatException | DateTimeParseException e) {
            Logging.PARSER.error("Failed to parse line" + entry.toString() + " from file " + fileName + " due to exception: ", e);
        }
        return parsedExpenditure;
    }

    @Nonnull
    private static LocalDate parseDateFromValue(@Nonnull final String dateString, @Nonnull final DateTimeFormatter formatter) throws DateTimeParseException {
        try {
            return LocalDate.parse(dateString, formatter);
        }
        catch (final DateTimeParseException e) {
            Logging.PARSER.error("Failed to parse date " + dateString);
            throw e;
        }
    }
}
