package com.expenditures.parsers;

import javax.annotation.Nonnull;
import java.util.List;

public class ParsedFile {

    public enum AccountType {
        AMEX(0),
        VISA(1),
        BANK_OF_AMERICA(2),
        SCHWAB(3),
        MASTERCARD(4);

        private final int value;

        AccountType(final int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private final List<ParsedExpenditure> entries;
    private final AccountType accountType;

    ParsedFile(@Nonnull final List<ParsedExpenditure> entries,
               @Nonnull final AccountType accountType) {
        this.entries = entries;
        this.accountType = accountType;
    }

    @Nonnull
    public List<ParsedExpenditure> getEntries() {
        return entries;
    }

    @Nonnull
    public AccountType getAccountType() {
        return accountType;
    }

}
