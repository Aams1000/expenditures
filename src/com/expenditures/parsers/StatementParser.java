package com.expenditures.parsers;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.List;

/**
 * Base interface for all parsers to implement.
 *
 * Created by Andrew on 1.6.19.
 */
public interface StatementParser {

//    boolean isCorrectFormat(@Nonnull final String line);

    @Nonnull
    List<Object[]> parse(@Nonnull final File file) throws UnexpectedFormatException;

//    @Nonnull
//    List<Object[]> recursiveParse(@Nonnull final File file, @Nonnull final ImmutableList.Builder<Object[]> builder) throws UnexpectedFormatException;
}
