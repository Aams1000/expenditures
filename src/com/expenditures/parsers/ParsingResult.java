package com.expenditures.parsers;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.util.List;

class ParsingResult {

    private final List<ParsedFile> parsedFiles;
    private final List<FailedFileWithReason> failedFileWithReasons;
    private final List<String> skippedFiles;

    private ParsingResult(@Nonnull final List<ParsedFile> parsedFiles,
                          @Nonnull final List<FailedFileWithReason> failedFileWithReasons,
                          @Nonnull final List<String> skippedFiles) {
        this.failedFileWithReasons = failedFileWithReasons;
        this.skippedFiles = skippedFiles;
        this.parsedFiles = parsedFiles;
    }

    @Nonnull
    List<ParsedExpenditure> getEntries() {
        final ImmutableList.Builder<ParsedExpenditure> builder = new ImmutableList.Builder<>();
        parsedFiles.forEach(file -> builder.addAll(file.getEntries()));
        return builder.build();
    }

    @Nonnull
    List<ParsedFile> getParsedFiles() {
        return parsedFiles;
    }
    @Nonnull
    List<FailedFileWithReason> getFailedFileWithReasons() {
        return failedFileWithReasons;
    }

    @Nonnull
    List<String> getSkippedFiles() {
        return skippedFiles;
    }

    static class Builder {

        final ImmutableList.Builder<ParsedFile> parsedFiles = new ImmutableList.Builder<>();
        final ImmutableList.Builder<FailedFileWithReason> failedFilesWithReasons = new ImmutableList.Builder<>();
        final ImmutableList.Builder<String> skippedFiles = new ImmutableList.Builder<>();

        Builder(){}

        @Nonnull
        ParsingResult build() {
            return new ParsingResult(parsedFiles.build(), failedFilesWithReasons.build(), skippedFiles.build());
        }

        @Nonnull
        Builder addFailedFileWithReason(@Nonnull final FailedFileWithReason failedFileWithReason) {
            failedFilesWithReasons.add(failedFileWithReason);
            return this;
        }

        @Nonnull
        Builder addSkippedFile(@Nonnull final String fileName) {
            skippedFiles.add(fileName);
            return this;
        }

        @Nonnull
        Builder addParsedFile(@Nonnull final ParsedFile parsedFile) {
            parsedFiles.add(parsedFile);
            return this;
        }
    }
}
