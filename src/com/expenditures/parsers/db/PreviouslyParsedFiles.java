package com.expenditures.parsers.db;

import com.expenditures.db.DatabaseConnections;
import com.expenditures.db.PreparedStatements;
import com.expenditures.db.QueryParam;
import com.expenditures.servlet.Logging;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Reads table from database and returns it as a map. Can also update the table with new entries.
 *
 * Can create a new table as well.
 *
 * Created by Andrew on 1.7.19.
 */
public class PreviouslyParsedFiles {

    private PreviouslyParsedFiles(){}

    @Nonnull
    public static Map<String, Long> queryFileNamesAndDateLastModified() {
        final ImmutableMap.Builder<String, Long> builder = new ImmutableMap.Builder<>();
        final ParsingQuery queryInfo = ParsingQuery.QUERY_PARSED_FILES;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                final String fileName = resultSet.getString(queryInfo.getResultIndex(QueryParam.FILE_NAME));
                final long timestamp = resultSet.getLong(queryInfo.getResultIndex(QueryParam.DATE_LAST_MODIFIED));
                builder.put(fileName, timestamp);
            }
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to query parsed files due to exception: ", e);
        }
        return builder.build();
    }

    public static DBUpdateResult updateParsedFiles(@Nonnull final Map<String, Long> fileNamesByDateModified) {
        try (final Connection connection = DatabaseConnections.getExpendituresConnection()) {
            backupParsedFiles(connection);
            deleteRowsThatWillBeReplaced(connection, fileNamesByDateModified);
            insertNewRows(connection, fileNamesByDateModified);
            return DBUpdateResult.SUCCESS;
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to update parsed files due to exception: " + e);
            return DBUpdateResult.FAILURE;
        }
    }

    private static void insertNewRows(@Nonnull final Connection connection,
                                      @Nonnull final Map<String, Long> fileNamesByDateModified) throws SQLException, InvalidKeyException {
        final ParsingQuery queryInfo = ParsingQuery.INSERT_PARSED_FILES;
        try (final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            for (final Map.Entry<String, Long> entry : fileNamesByDateModified.entrySet()) {
                PreparedStatements.setStrings(preparedStatement, queryInfo.getQueryIndices(QueryParam.FILE_NAME), entry.getKey());
                PreparedStatements.setLongs(preparedStatement, queryInfo.getQueryIndices(QueryParam.DATE_LAST_MODIFIED), entry.getValue());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        }
    }

    private static void deleteRowsThatWillBeReplaced(@Nonnull final Connection connection,
                                                     @Nonnull final Map<String, Long> fileNamesByDateModified) throws SQLException, InvalidKeyException {
        final ParsingQuery queryInfo = ParsingQuery.DELETE_PARSED_FILES_BY_NAME;
        try (final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            for (final Map.Entry<String, Long> entry : fileNamesByDateModified.entrySet()) {
                PreparedStatements.setStrings(preparedStatement, queryInfo.getQueryIndices(QueryParam.FILE_NAME), entry.getKey());
                PreparedStatements.setLongs(preparedStatement, queryInfo.getQueryIndices(QueryParam.DATE_LAST_MODIFIED), entry.getValue());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to delete parsed file rows due to exception: " + e);
            throw e;
        }
    }

    private static void backupParsedFiles(@Nonnull final Connection connection) throws SQLException {
        try (final PreparedStatement preparedStatement = connection.prepareStatement(ParsingQuery.BACKUP_PARSED_FILES.getQuery())) {
            preparedStatement.execute();
        }
        catch (final SQLException e) {
            Logging.PARSER.error("Failed to backup parsed files due to SQLException: ", e);
            throw e;
        }
    }
}
