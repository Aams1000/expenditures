package com.expenditures.parsers.db;

import com.expenditures.categories.ExpenditureCategory;
import com.expenditures.db.*;
import com.expenditures.parsers.ParsedExpenditure;
import com.expenditures.categories.CategoryLookup;
import com.expenditures.parsers.ParsedFile;
import com.expenditures.servlet.ExpendituresQuery;
import com.expenditures.servlet.Logging;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * Backs up and updates expenditures table.
 *
 * Created by Andrew on 1.8.19.
 */
public class ExpendituresUpdates {

    private ExpendituresUpdates() {}

    public static List<DBExpenditure> backupAndUpdateTable(@Nonnull final List<ParsedFile> parsedFiles) {
        List<DBExpenditure> insertedExpenditures = ImmutableList.of();
        try (final Connection connection = DatabaseConnections.getNonAutoCommitExpendituresConnection()) {
            backupTable(connection);
            final List<Integer> insertedIds = insertNewRows(connection, parsedFiles);
            insertedExpenditures = queryExpendituresFromIds(connection, insertedIds);
            try {
                connection.commit();
            }
            catch (final SQLException e) {
                Logging.PARSER.error("Failed to commit changes to database due to exception. Rolling back.", e);
                try {
                    connection.rollback();
                }
                catch (final SQLException e2) {
                    Logging.PARSER.error("Failed to rollback after failed commit. Something is severely wrong.", e2);
                }
            }
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to update parsed files due to exception: " + e);
        }
        return insertedExpenditures;
    }

    @Nonnull
    private static List<Integer> insertNewRows(@Nonnull final Connection connection,
                                               @Nonnull final List<ParsedFile> parsedFiles) throws SQLException, InvalidKeyException {
        final QueryInfo queryInfo = ParsingQuery.INSERT_EXPENDITURES;
        try (final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery(), Statement.RETURN_GENERATED_KEYS)) {
            for (final ParsedFile parsedFile : parsedFiles) {
                final List<ParsedExpenditure> expenditures = parsedFile.getEntries();
                final int accountType = parsedFile.getAccountType().getValue();
                for (final ParsedExpenditure entry : expenditures) {
                    PreparedStatements.setDates(preparedStatement, queryInfo.getQueryIndices(QueryParam.DATE_OF_TRANSACTION), entry.getTransactionDate());
                    PreparedStatements.setStrings(preparedStatement, queryInfo.getQueryIndices(QueryParam.MERCHANT), entry.getMerchant());
                    PreparedStatements.setInts(preparedStatement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), entry.getCategoryId());
                    PreparedStatements.setDoubles(preparedStatement, queryInfo.getQueryIndices(QueryParam.AMOUNT), entry.getAmount());
                    PreparedStatements.setInts(preparedStatement, queryInfo.getQueryIndices(QueryParam.ACCOUNT_TYPE), accountType);
                    preparedStatement.addBatch();
                }
            }
            preparedStatement.executeBatch();
            final List<Integer> generatedKeys = getGeneratedKeysFromResultSet(preparedStatement.getGeneratedKeys());
            return generatedKeys;
        }
    }

    @Nonnull
    private static List<Integer> getGeneratedKeysFromResultSet(@Nonnull final ResultSet resultSet) throws SQLException {
        final ImmutableList.Builder<Integer> builder = ImmutableList.builder();
        while (resultSet.next()) {
            builder.add(resultSet.getInt(1));
        }
        return builder.build();
    }

    @Nonnull
    private static List<DBExpenditure> queryExpendituresFromIds(@Nonnull final Connection connection,
                                                                @Nonnull final List<Integer> ids) throws SQLException , InvalidKeyException{
        final QueryInfo queryInfo = ExpendituresQuery.QUERY_EXPENDITURES_BY_ID;
        final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery());
        PreparedStatements.setArrays(preparedStatement, queryInfo.getQueryIndices(QueryParam.EXPENDITURE_ID), DatabaseConnections.convertIntListToSqlArray(connection, ids));
        final ResultSet resultSet = preparedStatement.executeQuery();

        final ImmutableList.Builder<DBExpenditure> builder = new ImmutableList.Builder<>();
        while (resultSet.next()) {
            final long id = resultSet.getLong(queryInfo.getResultIndex(QueryParam.EXPENDITURE_ID));
            final String merchant = resultSet.getString(queryInfo.getResultIndex(QueryParam.MERCHANT));
            final double amount = resultSet.getDouble(queryInfo.getResultIndex(QueryParam.AMOUNT));
            final LocalDate date = resultSet.getDate(queryInfo.getResultIndex(QueryParam.DATE_OF_TRANSACTION)).toLocalDate();
            final int categoryId = resultSet.getInt(queryInfo.getResultIndex(QueryParam.CATEGORY_ID));
            builder.add(new DBExpenditure(id, date, merchant, amount, categoryId));
        }
        return builder.build();
    }

    public static DBUpdateResult updateUncategorizedEntriesWithCategories(@Nonnull final Map<Long, ExpenditureCategory> idsToNewCategories) {
        try (final Connection connection = DatabaseConnections.getExpendituresConnection()) {
            backupTable(connection);
            updateCategoriesForExpenditureIds(connection, idsToNewCategories);
            return DBUpdateResult.SUCCESS;
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to update parsed files due to exception: " + e);
            return DBUpdateResult.FAILURE;
        }
    }

    private static void updateCategoriesForExpenditureIds(@Nonnull final Connection connection,
                                                          @Nonnull final Map<Long, ExpenditureCategory> idsToNewCategories) throws SQLException, InvalidKeyException {
        final QueryInfo queryInfo = ParsingQuery.UPDATE_EXPENDITURES_CATEGORIES;
        try (final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            for (final Map.Entry<Long, ExpenditureCategory> idAndCategory : idsToNewCategories.entrySet()) {
                PreparedStatements.setLongs(preparedStatement, queryInfo.getQueryIndices(QueryParam.EXPENDITURE_ID), idAndCategory.getKey());
                PreparedStatements.setStrings(preparedStatement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), idAndCategory.getValue().getName());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to update expenditures rows due to exception: " + e);
            throw e;
        }
    }

    private static void backupTable(@Nonnull final Connection connection) throws SQLException {
        try (final PreparedStatement preparedStatement = connection.prepareStatement(ParsingQuery.BACKUP_EXPENDITURES.getQuery())) {
            preparedStatement.execute();
        }
        catch (final SQLException e) {
            Logging.PARSER.error("Failed to backup expenditures table due to SQLException: ", e);
            throw e;
        }
    }

    @Nonnull
    public static List<DBExpenditure> queryUncategorizedExpenditures() {
        final ImmutableList.Builder<DBExpenditure> builder = new ImmutableList.Builder<>();
        final QueryInfo queryInfo = ParsingQuery.QUERY_UNCATEGORIZED_EXPENDITURES;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                final long id = resultSet.getLong(queryInfo.getResultIndex(QueryParam.EXPENDITURE_ID));
                final String merchant = resultSet.getString(queryInfo.getResultIndex(QueryParam.MERCHANT));
                final double amount = resultSet.getDouble(queryInfo.getResultIndex(QueryParam.AMOUNT));
                final LocalDate date = resultSet.getDate(queryInfo.getResultIndex(QueryParam.DATE_OF_TRANSACTION)).toLocalDate();
                builder.add(new DBExpenditure(id, date, merchant, amount, CategoryLookup.INSTANCE.getUnknownCategory().getId()));
            }
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to query uncategorized expenditures due to exception: ", e);
        }
        return builder.build();
    }
}