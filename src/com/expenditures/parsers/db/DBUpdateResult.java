package com.expenditures.parsers.db;

public enum DBUpdateResult {
    SUCCESS,
    FAILURE
}
