package com.expenditures.parsers.db;

import com.expenditures.categories.CategoryLookup;
import com.expenditures.db.QueryInfo;
import com.expenditures.db.QueryInfoProcessing;
import com.expenditures.db.QueryParam;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.util.List;

public enum ParsingQuery implements QueryInfo {

    QUERY_PARSED_FILES("select file_name, ds, id from t_parsed_files",

        ImmutableMap.of(),
        ImmutableMap.of(QueryParam.FILE_NAME, 1, QueryParam.DATE_LAST_MODIFIED, 2, QueryParam.FILE_ID, 3)
    ),

    BACKUP_PARSED_FILES("drop table if exists t_parsed_files_backup; " +
                        "create table t_parsed_files_backup (like t_parsed_files including all); " +
                        "insert into t_parsed_files_backup select * from t_parsed_files;",

            ImmutableMap.of(),
            ImmutableMap.of()
    ),

    DELETE_PARSED_FILES_BY_NAME("delete from t_parsed_files where file_name = ?",

            ImmutableMap.of(QueryParam.FILE_NAME, ImmutableList.of(1)),
            ImmutableMap.of()
    ),

    DELETE_PARSED_FILES_BY_ID("delete from t_parsed_files where id = ?",

            ImmutableMap.of(QueryParam.FILE_ID, ImmutableList.of(1)),
            ImmutableMap.of()
    ),

    INSERT_PARSED_FILES("insert into t_parsed_files values (default, ?, ?)",

            ImmutableMap.of(QueryParam.FILE_NAME, ImmutableList.of(1), QueryParam.DATE_LAST_MODIFIED, ImmutableList.of(2)),
            ImmutableMap.of()
    ),

    BACKUP_EXPENDITURES("drop table if exists t_expenditures_backup; " +
                        "create table t_expenditures_backup (like t_expenditures including all); " +
                        "insert into t_expenditures_backup select * from t_expenditures;",

            ImmutableMap.of(),
            ImmutableMap.of()
    ),

    INSERT_EXPENDITURES("with expenditures_insertion as (insert into t_expenditures values (default, ?, ?, ?, ?) returning id) " +
                        "insert into t_account_type select (select id from expenditures_insertion), ? " +
                        "where not exists (select 1 from t_expenditures e inner join t_account_type a on e.id = a.expenditure_id " +
                        "where e.ds = ? and e.merchant = ? and e.category = ? and e.amount = ? and a.type = ?)" +
                        "and not exists (select 1 from t_merchants_to_ignore m where m.merchant = ?);",

            ImmutableMap.of(QueryParam.DATE_OF_TRANSACTION, ImmutableList.of(1, 6), QueryParam.MERCHANT, ImmutableList.of(2, 7, 11), QueryParam.CATEGORY_ID, ImmutableList.of(3, 8), QueryParam.AMOUNT, ImmutableList.of(4, 9), QueryParam.ACCOUNT_TYPE, ImmutableList.of(5, 10)),
            ImmutableMap.of()
    ),

    QUERY_UNCATEGORIZED_EXPENDITURES("select id, ds, merchant, amount from t_expenditures where category = '" + CategoryLookup.UNKNOWN_CATEGORY_ID + "'",

            ImmutableMap.of(),
            ImmutableMap.of(QueryParam.EXPENDITURE_ID, 1, QueryParam.DATE_OF_TRANSACTION, 2, QueryParam.MERCHANT, 3, QueryParam.AMOUNT, 4)
    ),

    UPDATE_EXPENDITURES_CATEGORIES("update t_expenditures set category = ? where id = ?",

            ImmutableMap.of(QueryParam.CATEGORY_ID, ImmutableList.of(1), QueryParam.EXPENDITURE_ID, ImmutableList.of(2)),
            ImmutableMap.of()
    );

    private final String query;
    private final ImmutableMap<QueryParam, ImmutableList<Integer>> queryParamsByIndex;
    private final ImmutableMap<QueryParam, Integer> resultParamsByIndex;

    @Override
    @Nonnull
    public String getQuery() {
        return query;
    }

    @Override
    @Nonnull
    public List<Integer> getQueryIndices(@Nonnull QueryParam param) throws InvalidKeyException {
        return QueryInfoProcessing.getQueryIndices(param, queryParamsByIndex, this.name());
    }

    @Override
    public int getResultIndex(@Nonnull QueryParam param) throws InvalidKeyException {
        return QueryInfoProcessing.getResultIndex(param, resultParamsByIndex, this.name());
    }

    ParsingQuery(@Nonnull final String query,
                 @Nonnull final ImmutableMap<QueryParam, ImmutableList<Integer>> queryParamsByIndex,
                 @Nonnull final ImmutableMap<QueryParam, Integer> resultParamsByIndex) {
        this.query = query;
        this.queryParamsByIndex = queryParamsByIndex;
        this.resultParamsByIndex = resultParamsByIndex;
    }
}


