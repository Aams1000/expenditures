package com.expenditures.parsers;

import javax.annotation.Nonnull;

class FailedFileWithReason {

    private final String fileName;
    private final String reason;

    FailedFileWithReason(@Nonnull final String fileName, @Nonnull final String reason) {
        this.fileName = fileName;
        this.reason = reason;
    }

    @Nonnull
    String getFileName() {
        return fileName;
    }

    @Nonnull
    String getReason() {
        return reason;
    }
}
