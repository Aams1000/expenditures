package com.expenditures.util;

import javax.annotation.Nullable;

public class StringUtils {

    private StringUtils(){}

    public static boolean isBlank(@Nullable final String s) {
        return s == null || s.length() == 0 || s.replaceAll("\\s+", "").length() == 0;
    }
}
