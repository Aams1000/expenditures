package com.expenditures.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Creating this so all models inherit the desired JsonAutoDetect properties (having them implement this takes
 * less space than copy/pasting the annotations).
 *
 * Created by Andrew on 12.30.18.
 */

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE,
                getterVisibility = JsonAutoDetect.Visibility.NONE,
                setterVisibility = JsonAutoDetect.Visibility.NONE)
interface AutoDetectModel {
}
