package com.expenditures.models;

import com.expenditures.categories.ExpenditureCategory;
import com.expenditures.servlet.Logging;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Return object for the ExpendituresServlet. Enums correspond to names used on the front end.
 *
 * Created by Andrew on 12.15.18
 */

public class SummariesByCategory implements AutoDetectModel {

    public static final String JSON_LABEL = "summariesByCategory";

    private final Map<ExpenditureCategory, Summary> summariesByCategory;

    public SummariesByCategory(@Nonnull final Map<ExpenditureCategory, Double> spendingByCategory, final double totalSpending) {
        summariesByCategory = generateSummariesByCategory(spendingByCategory, totalSpending);
    }

    @Nonnull
    private static Map<ExpenditureCategory, Summary> generateSummariesByCategory(@Nonnull final Map<ExpenditureCategory, Double> spendingByCategory,
                                                                            final double totalSpending) {
        final ImmutableMap.Builder<ExpenditureCategory, Summary> builder = ImmutableMap.builder();
        spendingByCategory.forEach((key, value) -> builder.put(key, new Summary(value, value / totalSpending)));
        return builder.build();
    }

    @Nonnull
    public Map<ExpenditureCategory, Summary> getSummariesByCategory() {
        return summariesByCategory;
    }

    @Nonnull
    @JsonProperty("summariesByCategory")
    private Map<String, Summary> getDisplayContent() {
        try {
            return summariesByCategory.entrySet().stream()
                    .collect(Collectors.toMap(entry -> entry.getKey().getName(),
                            Map.Entry::getValue));
        }
        catch (final Exception e) {
            Logging.EXPENDITURES.error("Duplicate key", e);
            throw e;
        }
    }

    public static class Summary implements AutoDetectModel {

        @JsonProperty("amountSpent")
        private final double amountSpent;
        @JsonProperty("percentOfTotalSpending")
        private final double percentOfTotalSpending;

        Summary(final double amountSpent, final double percentOfTotalSpending) {
            this.amountSpent = amountSpent;
            this.percentOfTotalSpending = percentOfTotalSpending;
        }

        public double getAmountSpent() {
            return amountSpent;
        }

        public double getPercentOfTotalSpending() {
            return percentOfTotalSpending;
        }
    }

}
