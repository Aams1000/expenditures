package com.expenditures.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import java.time.Month;
import java.util.Map;

public class SummariesByCategoryByMonth implements AutoDetectModel {

    public static final String JSON_LABEL = "summariesByCategoryByMonth";

    @JsonProperty("summariesByCategoryByMonth")
    private final Map<Month, SummariesByCategory> summariesByCategoryByMonth;

    public SummariesByCategoryByMonth(@Nonnull final Map<Month, SummariesByCategory> summariesByCategoryByMonth) {
        this.summariesByCategoryByMonth = summariesByCategoryByMonth;
    }

    @Nonnull
    public Map<Month, SummariesByCategory> getSummariesByCategoryByMonth() {
        return summariesByCategoryByMonth;
    }
}
