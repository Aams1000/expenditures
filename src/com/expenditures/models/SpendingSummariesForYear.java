package com.expenditures.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;

public class SpendingSummariesForYear implements AutoDetectModel {

    public static final String JSON_LABEL = "spendingSummariesForYear";

    @JsonProperty("summariesByCategoryByMonth")
    private final SummariesByCategoryByMonth summariesByCategoryByMonth;
    @JsonProperty("summariesByCategoryForYear")
    private final SummariesByCategory summariesByCategoryForYear;
    @JsonProperty("averageSummaries")
    private final AverageSpendingSummaryForYear averageSpendingSummaryForYear;

    public SpendingSummariesForYear(@Nonnull final SummariesByCategoryByMonth summariesByCategoryByMonth,
                                    @Nonnull final SummariesByCategory summariesByCategoryForYear,
                                    @Nonnull final AverageSpendingSummaryForYear averageSpendingSummaryForYear) {
        this.summariesByCategoryByMonth = summariesByCategoryByMonth;
        this.summariesByCategoryForYear = summariesByCategoryForYear;
        this.averageSpendingSummaryForYear = averageSpendingSummaryForYear;
    }
}
