package com.expenditures.models;

import com.expenditures.categories.ExpenditureCategory;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import java.util.Map;

public class AverageSpendingSummaryForYear implements AutoDetectModel {

    @JsonProperty("summariesByCategory")
    private final Map<ExpenditureCategory, AverageSpendingSummary> summariesByCategory;

    public AverageSpendingSummaryForYear(@Nonnull final Map<ExpenditureCategory, AverageSpendingSummary> summariesByCategory) {
        this.summariesByCategory = summariesByCategory;
    }

}
