package com.expenditures.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class AverageSpendingSummary implements AutoDetectModel {

    @JsonProperty("averageSpending")
    private final double averageSpending;
    @JsonProperty("averagePercentOfTotalSpending")
    private final double averagePercentOfTotalSpending;
    @JsonProperty("numNonZeroMonths")
    private final int numNonZeroMonths;
    @JsonProperty("maxSpending")
    private final double maxSpending;
    @JsonProperty("monthOfMaxSpending")
    private final Month monthOfMaxSpending;
    @JsonProperty("totalAmountSpentOverPeriod")
    private final double totalAmountSpentOverPeriod;

    private AverageSpendingSummary(final double averageSpending,
                                   final double averagePercentOfTotalSpending,
                                   final int numNonZeroMonths,
                                   final double maxSpending,
                                   final Month monthOfMaxSpending,
                                   final double totalAmountSpentOverPeriod) {

        this.averageSpending = averageSpending;
        this.averagePercentOfTotalSpending = averagePercentOfTotalSpending;
        this.numNonZeroMonths = numNonZeroMonths;
        this.maxSpending = maxSpending;
        this.monthOfMaxSpending = monthOfMaxSpending;
        this.totalAmountSpentOverPeriod = totalAmountSpentOverPeriod;
    }

    public static class Builder {

        private final List<Double> spendingAmounts = new ArrayList<>();
        private final List<Double> percentsOfTotalSpending = new ArrayList<>();
        private double maxSpending = 0.0;
        private Month monthOfMaxSpending = null;
        private double overallSpending = 0.0;
        private int numMonthsInPeriod = Month.values().length;

        @Nonnull
        public Builder addSpendingForTimeUnit(@Nonnull final Month month,
                                              final double totalSpendingForPeriod,
                                              final double percentOfTotalSpendingForPeriod) {
            spendingAmounts.add(totalSpendingForPeriod);
            percentsOfTotalSpending.add(percentOfTotalSpendingForPeriod);
            overallSpending += totalSpendingForPeriod;
            if (totalSpendingForPeriod > maxSpending) {
                maxSpending = totalSpendingForPeriod;
                monthOfMaxSpending = month;
            }
            return this;
        }

        @Nonnull
        public Builder setNumMonthsInPeriod(final int numMonthsInPeriod) {
            this.numMonthsInPeriod = numMonthsInPeriod;
            return this;
        }

        @Nonnull
        public AverageSpendingSummary build() {

            final double averageSpending;
            final double averagePercentOfTotalSpending;
            int numNonZeroMonths = 0;

            if (!spendingAmounts.isEmpty()) {
                double sumAverageSpendings = 0;
                double sumPercentTotalSpendings = 0;
                for (int i = 0; i < spendingAmounts.size(); i++) {
                    final double averageSpendingForMonth = spendingAmounts.get(i);
                    sumAverageSpendings += averageSpendingForMonth;
                    sumPercentTotalSpendings += percentsOfTotalSpending.get(i);
                    if (averageSpendingForMonth > 0) {
                        numNonZeroMonths ++;
                    }
                }
                averageSpending = sumAverageSpendings / (double) numMonthsInPeriod;
                averagePercentOfTotalSpending = sumPercentTotalSpendings / (double) numMonthsInPeriod;
            }
            else {
                averageSpending = 0;
                averagePercentOfTotalSpending = 0;
            }

            return new AverageSpendingSummary(averageSpending, averagePercentOfTotalSpending, numNonZeroMonths, maxSpending, monthOfMaxSpending, overallSpending);
        }
    }
}
