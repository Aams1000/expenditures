package com.expenditures.categories;

import com.expenditures.parsers.db.DBUpdateResult;
import com.expenditures.servlet.Logging;

import javax.annotation.Nonnull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.util.Optional;

/**
 * Servlet to field category-related requests.
 *
 * Created by Andrew on 1.6.19.
 */
@Path("/categories")
public class CategoriesServlet {

    @QueryParam("requestType")
    private String m_requestType;

    @QueryParam("idToDelete")
    private int m_idToDelete;

    @QueryParam("replacementId")
    private int m_replacementId;

    @QueryParam("name")
    private String m_name;

    private enum RequestType {
        ADD_CATEGORY("add"),
        REMOVE_CATEGORY("remove");

        private final String name;

        RequestType(@Nonnull final String name) {
            this.name = name;
        }

        @Nonnull
        String getName() {
            return name;
        }

        static Optional<RequestType> fromString(@Nonnull final String name) {
            Optional<RequestType> matchingType = Optional.empty();
            for (final RequestType requestType : values()) {
                if (requestType.getName().equals(name)) {
                    matchingType = Optional.of(requestType);
                }
            }
            return matchingType;
        }
    }

    @GET
    public String actualTest() {
        return "GET method working.";
    }

    @POST
    public String testPost() {
        return "POST method working";
    }

    private String processPostRequest(@Nonnull final RequestType requestType) {
        final String message;
        if (RequestType.ADD_CATEGORY.equals(requestType)) {
            if (m_name != null && !CategoryLookup.INSTANCE.isExistingCategory(m_name)) {
                message = addNewCategory(m_name, CategoryLookup.INSTANCE);
            }
            else {
                message = "Invalid category name '" + m_name + "' is null or already exists in database.";
                Logging.CATEGORIES.info(message);
            }
        }
        else if (RequestType.REMOVE_CATEGORY.equals(requestType)) {
            if (CategoryLookup.INSTANCE.isExistingAndCustomCategory(m_idToDelete) && m_idToDelete != m_replacementId) {
                final int replacementId = CategoryLookup.INSTANCE.isExistingCategory(m_replacementId)
                        ? m_replacementId
                        : CategoryLookup.INSTANCE.getUnknownCategory().getId();
                message = removeCategoryAndUpdateTables(m_idToDelete, replacementId, CategoryLookup.INSTANCE);
            }
            else {
                message = "Deletion category EXPENDITURE_ID " + m_idToDelete + " either does not exists or is the same as replacement EXPENDITURE_ID " + m_replacementId;
                Logging.CATEGORIES.info(message);
            }
        }
        else {
            message = "Unidentified RequestType " + requestType.getName() + " received via POST request. Doing nothing.";
        }
        return message;
    }

    @Nonnull
    private static String addNewCategory(@Nonnull final String name, @Nonnull final CategoryLookup categoryLookup) {
        final DBUpdateResult result = CategoryUpdates.insertNewCategory(name, categoryLookup);
        final String message;
        if (DBUpdateResult.SUCCESS.equals(result)) {
            message = "Successfully inserted category '" + name + "' into database.";
            Logging.CATEGORIES.info(message);
        }
        else {
            message = "Failed to insert category '" + name + "' into database. Please check logs.";
            Logging.CATEGORIES.error(message);
        }
        return message;
    }

    @Nonnull
    private static String removeCategoryAndUpdateTables(final int idToDelete, final int replacementId, @Nonnull final CategoryLookup categoryLookup) {
        final DBUpdateResult result = CategoryUpdates.deleteCategoryAndUpdateEntries(idToDelete, replacementId, categoryLookup);
        final String message;
        if (DBUpdateResult.SUCCESS.equals(result)) {
            message = "Successfully removed category EXPENDITURE_ID " + idToDelete + " and replaced it with category EXPENDITURE_ID " + replacementId;
            Logging.CATEGORIES.info(message);
        }
        else {
            message = "Failed to remove category EXPENDITURE_ID " + idToDelete + " and replace it with " + replacementId;
            Logging.CATEGORIES.error(message);
        }
        return message;
    }
}
