package com.expenditures.categories;

import com.expenditures.db.QueryInfo;
import com.expenditures.db.QueryInfoProcessing;
import com.expenditures.db.QueryParam;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.util.List;

public enum CategoryQuery implements QueryInfo {

    QUERY_CATEGORY_IDS_AND_NAMES("select id, name, is_custom from t_categories",

            ImmutableMap.of(),
            ImmutableMap.of(QueryParam.EXPENDITURE_ID, 1, QueryParam.CATEGORY_NAME, 2, QueryParam.IS_CUSTOM, 3)
    ),

    INSERT_NEW_CATEGORY("insert into t_categories values (default, ?, TRUE)",

            ImmutableMap.of(QueryParam.CATEGORY_NAME, ImmutableList.of(1)),
            ImmutableMap.of()
    ),

    DELETE_CUSTOM_CATEGORY("delete from t_categories where id = ? and is_custom = TRUE;" +
            "update t_expenditures set category = ? where category = ?;"  +
            "update t_expenditures_backup set category = ? where category = ?;",

            ImmutableMap.of(QueryParam.ID_TO_DELETE, ImmutableList.of(1, 3, 5), QueryParam.REPLACEMENT_ID, ImmutableList.of(2, 4)),
            ImmutableMap.of()
    );

    private final String query;
    private final ImmutableMap<QueryParam, ImmutableList<Integer>> queryParamsByIndex;
    private final ImmutableMap<QueryParam, Integer> resultParamsByIndex;

    @Override
    @Nonnull
    public String getQuery() {
        return query;
    }

    @Override
    @Nonnull
    public List<Integer> getQueryIndices(@Nonnull QueryParam param) throws InvalidKeyException {
        return QueryInfoProcessing.getQueryIndices(param, queryParamsByIndex, this.name());
    }

    @Override
    public int getResultIndex(@Nonnull QueryParam param) throws InvalidKeyException {
        return QueryInfoProcessing.getResultIndex(param, resultParamsByIndex, this.name());
    }

    CategoryQuery(@Nonnull final String query,
                  @Nonnull final ImmutableMap<QueryParam, ImmutableList<Integer>> queryParamsByIndex,
                  @Nonnull final ImmutableMap<QueryParam, Integer> resultParamsByIndex) {
        this.query = query;
        this.queryParamsByIndex = queryParamsByIndex;
        this.resultParamsByIndex = resultParamsByIndex;
    }
}
