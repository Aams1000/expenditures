package com.expenditures.categories;

import com.expenditures.db.DatabaseConnections;
import com.expenditures.db.PreparedStatements;
import com.expenditures.db.QueryParam;
import com.expenditures.parsers.db.DBUpdateResult;
import com.expenditures.servlet.Logging;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

class CategoryUpdates {

    private CategoryUpdates(){}

    static DBUpdateResult insertNewCategory(@Nonnull final String name, @Nonnull final CategoryLookup categoryLookup) {
        final CategoryQuery queryInfo = CategoryQuery.INSERT_NEW_CATEGORY;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            PreparedStatements.setStrings(preparedStatement, queryInfo.getQueryIndices(QueryParam.CATEGORY_NAME), name);
            preparedStatement.execute();
            categoryLookup.refreshCategoriesFromDatabase();
            return DBUpdateResult.SUCCESS;
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to add new category due to exception: " + e);
            return DBUpdateResult.FAILURE;
        }
    }

    static DBUpdateResult deleteCategoryAndUpdateEntries(final int idToDelete,
                                                         final int replacementId,
                                                         @Nonnull final CategoryLookup categoryLookup) {
        final CategoryQuery queryInfo = CategoryQuery.DELETE_CUSTOM_CATEGORY;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            PreparedStatements.setInts(preparedStatement, queryInfo.getQueryIndices(QueryParam.ID_TO_DELETE), idToDelete);
            PreparedStatements.setInts(preparedStatement, queryInfo.getQueryIndices(QueryParam.REPLACEMENT_ID), replacementId);
            preparedStatement.execute();
            categoryLookup.refreshCategoriesFromDatabase();
            return DBUpdateResult.SUCCESS;
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to remove category and update tables due to exception: " + e);
            return DBUpdateResult.FAILURE;
        }
    }
}
