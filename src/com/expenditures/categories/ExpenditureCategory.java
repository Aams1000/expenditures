package com.expenditures.categories;

import javax.annotation.Nonnull;

public class ExpenditureCategory {

    private final int id;
    private final String name;

    public ExpenditureCategory(final int id, @Nonnull final String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Nonnull
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ExpenditureCategory
                && ((ExpenditureCategory) o).getName().equals(name)
                && ((ExpenditureCategory) o).getId() == id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
