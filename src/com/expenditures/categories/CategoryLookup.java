package com.expenditures.categories;

import com.expenditures.db.DatabaseConnections;
import com.expenditures.db.QueryParam;
import com.expenditures.servlet.Logging;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * For at least the first iteration, this will be a singleton. Considering other options for future revisions.
 *
 * Created by Andrew on 1.19.19
 */
public class CategoryLookup {

    public static final CategoryLookup INSTANCE = new CategoryLookup();

    public static final int UNKNOWN_CATEGORY_ID = 22;

    // special case of a category. Its EXPENDITURE_ID is changing around right now, so this is the safer way to develop
    // Might still keep this, though
    private static final String UNKNOWN_CATEGORY_NAME = "Unknown";

    //deliberately not final
    private Map<Integer, String> idsToName;
    private Map<String, Integer> namesToId;
    private Set<Integer> customCategories;

    private CategoryLookup() {
        try {
            refreshCategoriesFromDatabase();
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.CATEGORIES.error("Failed to initialize categories from database. Failing.", e);
            throw new RuntimeException(e);
        }
    }

    public void refreshCategoriesFromDatabase() throws SQLException, InvalidKeyException {
        final CategoryQuery queryInfo = CategoryQuery.QUERY_CATEGORY_IDS_AND_NAMES;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            final ImmutableMap.Builder<Integer, String> idsToNameBuilder = new ImmutableMap.Builder<>();
            final ImmutableMap.Builder<String, Integer> namesToIdBuilder = new ImmutableMap.Builder<>();
            final ImmutableSet.Builder<Integer> customSetBuilder = new ImmutableSet.Builder<>();
            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                final int id = resultSet.getInt(queryInfo.getResultIndex(QueryParam.EXPENDITURE_ID));
                final String name = resultSet.getString(queryInfo.getResultIndex(QueryParam.CATEGORY_NAME));
                final boolean isCustom = resultSet.getBoolean(queryInfo.getResultIndex(QueryParam.IS_CUSTOM));
                idsToNameBuilder.put(id, name);
                namesToIdBuilder.put(name, id);
                if (isCustom) {
                    customSetBuilder.add(id);
                }
            }
            idsToName = idsToNameBuilder.build();
            namesToId = namesToIdBuilder.build();
            customCategories = customSetBuilder.build();
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.CATEGORIES.error("Failed to retrieve categories from database due to exception: ", e);
            throw e;
        }
    }

    public Optional<ExpenditureCategory> getCategoryFromId(final int id) {
        final String name = idsToName.get(id);
        return name != null ? Optional.of(new ExpenditureCategory(id, name)) : Optional.empty();
    }

    public Optional<ExpenditureCategory> getCategoryFromName(@Nonnull final String name) {
        final int id = namesToId.getOrDefault(name, -1);
        return id >= 0 ? Optional.of(new ExpenditureCategory(id, name)) : Optional.empty();
    }

    public ExpenditureCategory getUnknownCategory() {
        final Optional<ExpenditureCategory> unknownCategory = getCategoryFromName(UNKNOWN_CATEGORY_NAME);
        if (!unknownCategory.isPresent()) {
            final String message = "Unknown category is not present. Something is wrong.";
            Logging.CATEGORIES.error("Unknown category is not present. Something is wrong.");
            throw new RuntimeException(message);
        }
        else {
            return unknownCategory.get();
        }
    }

    public boolean isExistingCategory(@Nonnull final String name) {
        return namesToId.containsKey(name);
    }

    public boolean isExistingCategory(final int categoryId) {
        return idsToName.containsKey(categoryId);
    }

    public boolean isExistingAndCustomCategory(final int categoryId) {
        return idsToName.containsKey(categoryId) && customCategories.contains(categoryId);
    }

}
