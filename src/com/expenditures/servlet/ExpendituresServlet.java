package com.expenditures.servlet;

import com.expenditures.categories.CategoryLookup;
import com.expenditures.categories.ExpenditureCategory;
import com.expenditures.models.AverageSpendingSummaryForYear;
import com.expenditures.models.SpendingSummariesForYear;
import com.expenditures.models.SummariesByCategory;
import com.expenditures.models.SummariesByCategoryByMonth;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Path("/")
public class ExpendituresServlet {

    private static final LocalDate TEST_START_DATE = LocalDate.of(2016, Month.MARCH, 1);
    private static final LocalDate TEST_END_DATE = LocalDate.of(2016, Month.OCTOBER, 31);

    private static final List<ExpenditureCategory> TEST_CATEGORIES = ImmutableList.of(CategoryLookup.INSTANCE.getCategoryFromName("Groceries").get(),
            CategoryLookup.INSTANCE.getCategoryFromName("Restaurants").get(),
            CategoryLookup.INSTANCE.getCategoryFromName("Housing & Rent").get());

    @GET
    @Nonnull
    public String generateSpendingSummaries() {
        final Map<String, Object> returnMap = new HashMap<>();
        final Optional<SummariesByCategory> summariesByCategory = retrieveExpendituresOverInterval(TEST_CATEGORIES, TEST_START_DATE, TEST_END_DATE);
        if (summariesByCategory.isPresent()) {
            returnMap.put(SummariesByCategory.JSON_LABEL, summariesByCategory.get());
        }
        else {
            returnMap.put("Failed to query summaries by category.", null);
        }
        final Optional<SpendingSummariesForYear> monthlySummaries = retrieveYearTotalsAndByMonth(TEST_CATEGORIES, TEST_END_DATE);
        if (monthlySummaries.isPresent()) {
            returnMap.put(SpendingSummariesForYear.JSON_LABEL, monthlySummaries.get());
        }
        else {
            returnMap.put("Failed to query monthly summaries.", null);
        }
        final ObjectWriter mapper = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonString = null;
        try {
            jsonString = mapper.writeValueAsString(returnMap);
        }
        catch (final JsonProcessingException e) {
            jsonString =  "Failed to convert object to JSON: " + e.getMessage();
        }
        return jsonString;
    }

    @Nonnull
    private static Optional<SummariesByCategory> retrieveExpendituresOverInterval(@Nonnull final List<ExpenditureCategory> categories,
                                                                                  @Nonnull final LocalDate startDate,
                                                                                  @Nonnull final LocalDate endDate) {
        Optional<SummariesByCategory> summariesByCategory = Optional.empty();
        final Optional<ExpendituresDAO.SpendingByCategory> queryResult = ExpendituresDAO.retrieveExpendituresByCategoryForInterval(categories, startDate, endDate);
        if (queryResult.isPresent()) {
            summariesByCategory = Optional.of(generateSummariesFromQueryResult(queryResult.get()));
        }
        return summariesByCategory;
    }

    @Nonnull
    private static Optional<SpendingSummariesForYear> retrieveYearTotalsAndByMonth(@Nonnull final List<ExpenditureCategory> categories,
                                                                                   @Nonnull final LocalDate endDate) {
        Optional<SpendingSummariesForYear> summariesByMonth = Optional.empty();
        //Hmm, as much as I dislike magic numbers, the first of the year sounds like a reasonable place to hardcode a 1...
        final LocalDate firstOfYear = LocalDate.of(endDate.getYear(), Month.JANUARY.getValue(), 1);
        final Optional<Map<Month, ExpendituresDAO.SpendingByCategory>> monthlySummaryQueryResult = ExpendituresDAO.retrieveSpendingByCategoryPerMonth(categories, firstOfYear, endDate);
        //avoiding ImmutableMap here as I'm pretty sure I recall Jackson having issues serializing them
        if (monthlySummaryQueryResult.isPresent()) {
            final Map<Month, SummariesByCategory> summariesMap = new HashMap<>();
            monthlySummaryQueryResult.get().forEach((month, spendingByCategory) -> summariesMap.put(month, generateSummariesFromQueryResult(spendingByCategory)));
            final SummariesByCategory summaryForYear = SpendingIntervalProcessor.generateYearlySummariesFromMonthlySummaries(summariesMap);
            final SummariesByCategoryByMonth summariesByCategoryByMonth = new SummariesByCategoryByMonth(summariesMap);
            final AverageSpendingSummaryForYear averageSummaries = SpendingIntervalProcessor.generateAverageSpendingPerMonth(summariesByCategoryByMonth);
            summariesByMonth = Optional.of(new SpendingSummariesForYear(summariesByCategoryByMonth, summaryForYear, averageSummaries));
        }
        return summariesByMonth;
    }

    @Nonnull
    private static SummariesByCategory generateSummariesFromQueryResult(@Nonnull final ExpendituresDAO.SpendingByCategory queryResult) {
        return new SummariesByCategory(queryResult.getSpendingByCategory(), queryResult.getTotalSpending());
    }
}
