package com.expenditures.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Logging {

    private Logging(){}

    public static final Logger CATEGORIES = LogManager.getLogger("CATEGORIES");
    public static final Logger EXPENDITURES = LogManager.getLogger("EXPENDITURES");
    public static final Logger PARSER = LogManager.getLogger("PARSER");
    public static final Logger MERCHANTS = LogManager.getLogger("MERCHANTS");
}
