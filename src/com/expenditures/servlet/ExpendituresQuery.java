package com.expenditures.servlet;

import com.expenditures.db.QueryInfo;
import com.expenditures.db.QueryInfoProcessing;
import com.expenditures.db.QueryParam;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.util.List;

/**
 * Would be nice to include a SQL generator that could convert these into Java objects, but not necessary at the moment.
 *
 * This class is over-designed. This stuff should probably live in config if I want to make a system like this.
 *
 * Created by Andrew on 7.27.18
 */
public enum ExpendituresQuery implements QueryInfo {

    QUERY_EXPENDITURES_FOR_INTERVAL("select category, sum(amount) " +
                                        "from t_expenditures e " +
                                        "where e.ds >= ? " +
                                        "and e.ds <= ? " +
                                        "and e.category = any(?::integer[]) " +
                                        "and not exists (select 1 from t_merchants_to_ignore m where m.merchant = e.merchant) " +
                                        "group by category",

            ImmutableMap.of(QueryParam.START_DATE, ImmutableList.of(1), QueryParam.END_DATE, ImmutableList.of(2), QueryParam.CATEGORY_ID, ImmutableList.of(3)),
            ImmutableMap.of(QueryParam.CATEGORY_ID, 1, QueryParam.AMOUNT, 2)
    ),

    QUERY_EXPENDITURES_BY_MONTH("select extract(month from ds) as month, category, sum(amount) " +
                                "from t_expenditures e " +
                                "where e.ds >= ? " +
                                "and e.ds <= ? " +
                                "and e.category = any(?::integer[]) " +
                                "and not exists (select 1 from t_merchants_to_ignore m where m.merchant = e.merchant) " +
                                "group by month, category",

            ImmutableMap.of(QueryParam.START_DATE, ImmutableList.of(1), QueryParam.END_DATE, ImmutableList.of(2), QueryParam.CATEGORY_ID, ImmutableList.of(3)),
            ImmutableMap.of(QueryParam.MONTH, 1, QueryParam.CATEGORY_ID, 2, QueryParam.AMOUNT, 3)
    ),

    QUERY_EXPENDITURES_BY_ID("select id, ds, merchant, category, amount " +
                             "from t_expenditures " +
                             "where id = any(?::integer[])",

            ImmutableMap.of(QueryParam.EXPENDITURE_ID, ImmutableList.of(1)),
            ImmutableMap.of(QueryParam.EXPENDITURE_ID, 1, QueryParam.DATE_OF_TRANSACTION, 2, QueryParam.MERCHANT, 3, QueryParam.CATEGORY_ID, 4, QueryParam.AMOUNT, 5)
    ),

    DELETE_EXPENDITURES("delete from t_expenditures where id = ?",

            ImmutableMap.of(QueryParam.EXPENDITURE_ID, ImmutableList.of(1)),
            ImmutableMap.of()
    );

    private final String query;
    private final ImmutableMap<QueryParam, ImmutableList<Integer>> queryParamsByIndex;
    private final ImmutableMap<QueryParam, Integer> resultParamsByIndex;

    @Override
    @Nonnull
    public String getQuery() {
        return query;
    }

    @Override
    @Nonnull
    public List<Integer> getQueryIndices(@Nonnull QueryParam param) throws InvalidKeyException {
        return QueryInfoProcessing.getQueryIndices(param, queryParamsByIndex, this.name());
    }

    @Override
    public int getResultIndex(@Nonnull QueryParam param) throws InvalidKeyException {
        final Integer index = resultParamsByIndex.get(param);
        if (index == null) {
            throw new InvalidKeyException("Query result " + this.name() + " does not support the parameter " + param.name());
        }
        return index;
    }

    ExpendituresQuery(@Nonnull final String query,
                      @Nonnull final ImmutableMap<QueryParam, ImmutableList<Integer>> queryParamsByIndex,
                      @Nonnull final ImmutableMap<QueryParam, Integer> resultParamsByIndex) {
        this.query = query;
        this.queryParamsByIndex = queryParamsByIndex;
        this.resultParamsByIndex = resultParamsByIndex;
    }
}

