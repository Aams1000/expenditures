package com.expenditures.servlet;

import com.expenditures.categories.ExpenditureCategory;
import com.expenditures.models.AverageSpendingSummary;
import com.expenditures.models.AverageSpendingSummaryForYear;
import com.expenditures.models.SummariesByCategory;
import com.expenditures.models.SummariesByCategoryByMonth;

import javax.annotation.Nonnull;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

class SpendingIntervalProcessor {

    private SpendingIntervalProcessor(){}

    @Nonnull
    static AverageSpendingSummaryForYear generateAverageSpendingPerMonth(@Nonnull final SummariesByCategoryByMonth summariesByCategoryByMonth) {
        final Map<ExpenditureCategory, AverageSpendingSummary.Builder> buildersByCategory = new HashMap<>();
        final Map<Month, SummariesByCategory> summariesByMonth = summariesByCategoryByMonth.getSummariesByCategoryByMonth();
        final int numMonthsInPeriod = summariesByMonth.size();
        for (final Map.Entry<Month, SummariesByCategory> monthAndSummary : summariesByMonth.entrySet()) {
            final Month month = monthAndSummary.getKey();
            final SummariesByCategory summariesByCategory = monthAndSummary.getValue();
            for (final Map.Entry<ExpenditureCategory, SummariesByCategory.Summary> categoryAndSummary : summariesByCategory.getSummariesByCategory().entrySet()) {
                final ExpenditureCategory category = categoryAndSummary.getKey();
                final SummariesByCategory.Summary summary = categoryAndSummary.getValue();
                final AverageSpendingSummary.Builder builder = buildersByCategory.getOrDefault(category, new AverageSpendingSummary.Builder().setNumMonthsInPeriod(numMonthsInPeriod));
                builder.addSpendingForTimeUnit(month, summary.getAmountSpent(), summary.getPercentOfTotalSpending());
                buildersByCategory.put(category, builder);
            }
        }
        final Map<ExpenditureCategory, AverageSpendingSummary> summariesByCategory = buildersByCategory.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().build()));
        return new AverageSpendingSummaryForYear(summariesByCategory);
    }

    @Nonnull
    static SummariesByCategory generateYearlySummariesFromMonthlySummaries(@Nonnull final Map<Month, SummariesByCategory> summariesByMonth) {
        final Map<ExpenditureCategory, Double> spendingByCategory = new HashMap<>();
        double totalSpending = 0;
        //not streaming here due to the totalSpending double needing to be mutable
        for (final SummariesByCategory summaries : summariesByMonth.values()) {
            for (final Map.Entry<ExpenditureCategory, SummariesByCategory.Summary> summaryForCategory : summaries.getSummariesByCategory().entrySet()) {
                final ExpenditureCategory category = summaryForCategory.getKey();
                final double amountSpent = summaryForCategory.getValue().getAmountSpent();
                spendingByCategory.put(category, spendingByCategory.getOrDefault(category, 0.0) + amountSpent);
                totalSpending += amountSpent;
            }
        }
        return new SummariesByCategory(spendingByCategory, totalSpending);
    }
}
