package com.expenditures.servlet;

import com.expenditures.categories.CategoryLookup;
import com.expenditures.categories.ExpenditureCategory;
import com.expenditures.db.DatabaseConnections;
import com.expenditures.db.PreparedStatements;
import com.expenditures.db.QueryInfo;
import com.expenditures.db.QueryParam;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.sql.*;
import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Retrieves expenditure information as requested by the user.
 *
 * Created by Andrew on 7.27.18
 */
public class ExpendituresDAO {
//
//    V1:
//
//    Most recent month:
//
//    Absolute:
//    by category
//    pie chart
//
//    Year to date:
//
//    Absolute:
//    By category
//    Pie chart
//
//    Average values:
//    By category
//    Pie chart
//
//    V2:
//
//    Month:
//    Compared to average values
//    Projected spending?

    private ExpendituresDAO(){}

    @Nonnull
    static Optional<SpendingByCategory> retrieveExpendituresByCategoryForInterval(@Nonnull final Collection<ExpenditureCategory> categories,
                                                                                         @Nonnull final LocalDate startDate,
                                                                                         @Nonnull final LocalDate endDate) {
        Optional<SpendingByCategory> queryResult = Optional.empty();
        final QueryInfo queryInfo = ExpendituresQuery.QUERY_EXPENDITURES_FOR_INTERVAL;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            PreparedStatements.setDates(preparedStatement, queryInfo.getQueryIndices(QueryParam.START_DATE), startDate);
            PreparedStatements.setDates(preparedStatement, queryInfo.getQueryIndices(QueryParam.END_DATE), endDate);
            PreparedStatements.setArrays(preparedStatement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), DatabaseConnections.convertExpendituresToSqlArray(connection, categories));
            queryResult = Optional.of(SpendingByCategory.fromResultSet(preparedStatement.executeQuery(), queryInfo));
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.EXPENDITURES.error("Failed to query database due to exception: ", e);
        }
        return queryResult;
    }

    @Nonnull
    static Optional<Map<Month, SpendingByCategory>> retrieveSpendingByCategoryPerMonth(@Nonnull final Collection<ExpenditureCategory> categories,
                                                                                       @Nonnull final LocalDate startDate,
                                                                                       @Nonnull final LocalDate endDate) {
        Optional<Map<Month, SpendingByCategory>> spendingByMonth = Optional.empty();
        final QueryInfo queryInfo = ExpendituresQuery.QUERY_EXPENDITURES_BY_MONTH;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            PreparedStatements.setDates(preparedStatement, queryInfo.getQueryIndices(QueryParam.START_DATE), startDate);
            PreparedStatements.setDates(preparedStatement, queryInfo.getQueryIndices(QueryParam.END_DATE), endDate);
            PreparedStatements.setArrays(preparedStatement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), DatabaseConnections.convertExpendituresToSqlArray(connection, categories));
            spendingByMonth = Optional.of(generateSpendingByMonthFromResultSet(preparedStatement.executeQuery(), queryInfo));
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.EXPENDITURES.error("Failed to query database due to exception: ", e);
        }
        return spendingByMonth;
    }

    @Nonnull
    private static Map<Month, SpendingByCategory> generateSpendingByMonthFromResultSet(@Nonnull final ResultSet resultSet,
                                                                                       @Nonnull final QueryInfo queryInfo) throws SQLException, InvalidKeyException {
        final Map<Month, Map<ExpenditureCategory, Double>> expenditureMapByMonth = new HashMap<>();
        final Map<Month, Double> spendingByMonth = new HashMap<>();
        while (resultSet.next()) {
            final Month month = Month.of(resultSet.getInt(queryInfo.getResultIndex(QueryParam.MONTH)));
            final int categoryId = resultSet.getInt(queryInfo.getResultIndex(QueryParam.CATEGORY_ID));
            final Optional<ExpenditureCategory> category = CategoryLookup.INSTANCE.getCategoryFromId(categoryId);
            final double amount = resultSet.getDouble(queryInfo.getResultIndex(QueryParam.AMOUNT));
            if (category.isPresent()) {
                spendingByMonth.put(month, spendingByMonth.getOrDefault(month, 0.0) + amount);
                if (expenditureMapByMonth.containsKey(month)) {
                    expenditureMapByMonth.get(month).put(category.get(), amount);
                }
                else {
                    final Map<ExpenditureCategory, Double> mapForCurrentMonth = new HashMap<>();
                    mapForCurrentMonth.put(category.get(), amount);
                    expenditureMapByMonth.put(month, mapForCurrentMonth);
                }
            } else {
                Logging.EXPENDITURES.error("Unknown expenditure category " + categoryId + " retrieved from database.");
            }
        }
        final ImmutableMap.Builder<Month, SpendingByCategory> builder = ImmutableMap.builder();
        expenditureMapByMonth.forEach((month, spendingByCategory) -> builder.put(month, new SpendingByCategory(spendingByCategory, spendingByMonth.get(month))));
        return builder.build();
    }

    static class SpendingByCategory {

        private final Map<ExpenditureCategory, Double> spendingByCategory;
        private final double totalSpending;

        SpendingByCategory(@Nonnull final Map<ExpenditureCategory, Double> spendingByCategory, final double totalSpending) {
            this.spendingByCategory = spendingByCategory;
            this.totalSpending = totalSpending;
        }

        @Nonnull
        static SpendingByCategory fromResultSet(@Nonnull final ResultSet resultSet,
                                                @Nonnull final QueryInfo queryInfo) throws SQLException, InvalidKeyException {
            final ImmutableMap.Builder<ExpenditureCategory, Double> builder = ImmutableMap.builder();
            double totalSpending = 0;
            while (resultSet.next()) {
                final int categoryId = resultSet.getInt(queryInfo.getResultIndex(QueryParam.CATEGORY_ID));
                final Optional<ExpenditureCategory> expenditureCategory = CategoryLookup.INSTANCE.getCategoryFromId(categoryId);
                final double categorySpending = resultSet.getDouble(queryInfo.getResultIndex(QueryParam.AMOUNT));
                //would normally do an ifPresent lambda, but variables used must be immutable, and I want to add to the running sum
                if (expenditureCategory.isPresent()) {
                    totalSpending += categorySpending;
                    builder.put(expenditureCategory.get(), categorySpending);
                } else {
                    Logging.EXPENDITURES.error("Unknown expenditure category " + categoryId + " retrieved from database.");
                }
            }
            return new SpendingByCategory(builder.build(), totalSpending);
        }

        @Nonnull
        Map<ExpenditureCategory, Double> getSpendingByCategory() {
            return spendingByCategory;
        }

        double getTotalSpending() {
            return totalSpending;
        }
    }
}
