package com.expenditures.merchants.db;

import com.expenditures.categories.ExpenditureCategory;
import com.expenditures.db.DatabaseConnections;
import com.expenditures.db.PreparedStatements;
import com.expenditures.db.QueryInfo;
import com.expenditures.db.QueryParam;
import com.expenditures.parsers.db.DBUpdateResult;
import com.expenditures.servlet.Logging;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class MerchantsToIgnore {

    private MerchantsToIgnore(){}

    public static int addMerchantToIgnore(@Nonnull final List<String> merchantsToIgnore) {
        int numExpendituresAffected = 0;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection()) {
            addMerchants(connection, merchantsToIgnore);
            numExpendituresAffected = countExpendituresByMerchant(connection, merchantsToIgnore);
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.MERCHANTS.error("Failed to retrieve merchants by category due to exception: ", e);
        }
        return numExpendituresAffected;
    }

    private static void addMerchants(@Nonnull final Connection connection, @Nonnull final List<String> merchantsToIgnore) throws SQLException, InvalidKeyException {
        final QueryInfo queryInfo = MerchantQuery.INSERT_MERCHANT_TO_IGNORE;
        try (final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            for (final String merchant : merchantsToIgnore) {
                PreparedStatements.setStrings(preparedStatement, queryInfo.getQueryIndices(QueryParam.MERCHANT), merchant);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        }
        catch (final SQLException e) {
            Logging.MERCHANTS.error("Failed to insert new merchants to ignore due to SQLException: ", e);
            throw e;
        }
    }

    private static int countExpendituresByMerchant(@Nonnull final Connection connection, @Nonnull final List<String> merchants) throws SQLException, InvalidKeyException {
        final QueryInfo queryInfo = MerchantQuery.COUNT_EXPENDITURES_BY_MERCHANTS;
        try (final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            PreparedStatements.setArrays(preparedStatement, queryInfo.getQueryIndices(QueryParam.MERCHANT_ARRAY), DatabaseConnections.convertStringListToSqlArray(connection, merchants));
            final ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.getInt(queryInfo.getResultIndex(QueryParam.COUNT));
        }
        catch (final SQLException e) {
            Logging.MERCHANTS.error("Failed to query expenditures by merchant due to ignore due to SQLException: ", e);
            throw e;
        }
    }

    static DBUpdateResult insertMerchantsByCategory(@Nonnull final Map<String, ExpenditureCategory> MerchantsToIgnore) {
        final QueryInfo queryInfo = MerchantQuery.INSERT_INTO_MERCHANTS_BY_CATEGORY;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            backupMerchantsByCategory(connection);
            for (final Map.Entry<String, ExpenditureCategory> entry : MerchantsToIgnore.entrySet()) {
                PreparedStatements.setStrings(preparedStatement, queryInfo.getQueryIndices(QueryParam.MERCHANT), entry.getKey());
                PreparedStatements.setStrings(preparedStatement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), entry.getValue().getName());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            return DBUpdateResult.SUCCESS;
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.MERCHANTS.error("Failed to insert merchants by category due to exception: " + e);
            return DBUpdateResult.FAILURE;
        }
    }

    private static void backupMerchantsByCategory(@Nonnull final Connection connection) throws SQLException {
        try (final PreparedStatement preparedStatement = connection.prepareStatement(MerchantQuery.BACKUP_MERCHANTS_BY_CATEGORY.getQuery())) {
            preparedStatement.execute();
        }
        catch (final SQLException e) {
            Logging.MERCHANTS.error("Failed to backup merchants by category due to SQLException: ", e);
            throw e;
        }
    }
}
