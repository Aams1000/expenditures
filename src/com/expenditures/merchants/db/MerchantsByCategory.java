package com.expenditures.merchants.db;

import com.expenditures.categories.ExpenditureCategory;
import com.expenditures.db.DatabaseConnections;
import com.expenditures.db.PreparedStatements;
import com.expenditures.db.QueryInfo;
import com.expenditures.db.QueryParam;
import com.expenditures.categories.CategoryLookup;
import com.expenditures.parsers.db.DBUpdateResult;
import com.expenditures.servlet.Logging;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

/**
 * Reads table from database and returns it as a map. Can also update the table with new entries.
 *
 * Can create a new table as well.
 *
 * Created by Andrew on 1.7.19.
 */
public class MerchantsByCategory {

    private MerchantsByCategory(){}

    @Nonnull
    public static Map<String, ExpenditureCategory> queryMerchantsByCategory() {
        final ImmutableMap.Builder<String, ExpenditureCategory> builder = new ImmutableMap.Builder<>();
        final QueryInfo queryInfo = MerchantQuery.QUERY_MERCHANTS_BY_CATEGORY;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                final String merchant = resultSet.getString(queryInfo.getResultIndex(QueryParam.MERCHANT));
                final int categoryId = resultSet.getInt(queryInfo.getResultIndex(QueryParam.CATEGORY_ID));
                final Optional<ExpenditureCategory> category = CategoryLookup.INSTANCE.getCategoryFromId(categoryId);
                if (category.isPresent()) {
                    builder.put(merchant, category.get());
                } else {
                    Logging.EXPENDITURES.error("Unknown expenditure category " + categoryId + " retrieved from database.");
                }
            }
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to retrieve merchants by category due to exception: ", e);
        }
        return builder.build();
    }

    static DBUpdateResult insertMerchantsByCategory(@Nonnull final Map<String, ExpenditureCategory> merchantsByCategory) {
        final QueryInfo queryInfo = MerchantQuery.INSERT_INTO_MERCHANTS_BY_CATEGORY;
        try (final Connection connection = DatabaseConnections.getExpendituresConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getQuery())) {
            backupMerchantsByCategory(connection);
            for (final Map.Entry<String, ExpenditureCategory> entry : merchantsByCategory.entrySet()) {
                PreparedStatements.setStrings(preparedStatement, queryInfo.getQueryIndices(QueryParam.MERCHANT), entry.getKey());
                PreparedStatements.setStrings(preparedStatement, queryInfo.getQueryIndices(QueryParam.CATEGORY_ID), entry.getValue().getName());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            return DBUpdateResult.SUCCESS;
        }
        catch (final SQLException | InvalidKeyException e) {
            Logging.PARSER.error("Failed to insert merchants by category due to exception: " + e);
            return DBUpdateResult.FAILURE;
        }
    }

    private static void backupMerchantsByCategory(@Nonnull final Connection connection) throws SQLException {
        try (final PreparedStatement preparedStatement = connection.prepareStatement(MerchantQuery.BACKUP_MERCHANTS_BY_CATEGORY.getQuery())) {
            preparedStatement.execute();
        }
        catch (final SQLException e) {
            Logging.PARSER.error("Failed to backup merchants by category due to SQLException: ", e);
            throw e;
        }
    }
}
