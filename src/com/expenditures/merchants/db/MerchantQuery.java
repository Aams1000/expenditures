package com.expenditures.merchants.db;

import com.expenditures.db.QueryInfo;
import com.expenditures.db.QueryInfoProcessing;
import com.expenditures.db.QueryParam;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.util.List;

public enum MerchantQuery implements QueryInfo {

    QUERY_MERCHANTS_BY_CATEGORY("select merchant, category" +
            "from t_merchants_by_category",

            ImmutableMap.of(),
            ImmutableMap.of(QueryParam.MERCHANT, 1, QueryParam.CATEGORY_ID, 2)
    ),

    BACKUP_MERCHANTS_BY_CATEGORY("drop table if exists t_merchants_by_category_backup; " +
            "create table t_merchants_by_category_backup (like t_merchants_by_category including all); " +
            "insert into t_merchants_by_category_backup select * from t_merchants_by_category;",

            ImmutableMap.of(),
            ImmutableMap.of()
    ),

    INSERT_INTO_MERCHANTS_BY_CATEGORY("insert into t_merchants_by_category values (?, ?)",

            ImmutableMap.of(QueryParam.MERCHANT, ImmutableList.of(1), QueryParam.CATEGORY_ID, ImmutableList.of(2)),
            ImmutableMap.of()
    ),

    INSERT_MERCHANT_TO_IGNORE("insert into t_merchants_to_ignore values (?)",

            ImmutableMap.of(QueryParam.MERCHANT, ImmutableList.of(1)),
            ImmutableMap.of()
    ),

    DELETE_MERCHANT_TO_IGNORE("delete from t_merchants_to_ignore where merchant = ?",

            ImmutableMap.of(QueryParam.MERCHANT, ImmutableList.of(1)),
            ImmutableMap.of()
    ),

    COUNT_EXPENDITURES_BY_MERCHANTS("select count(*) from t_expenditures where merchant = any(?::text[])",

            ImmutableMap.of(QueryParam.MERCHANT_ARRAY, ImmutableList.of(1)),
            ImmutableMap.of(QueryParam.COUNT, 1)
    );

    private final String query;
    private final ImmutableMap<QueryParam, ImmutableList<Integer>> queryParamsByIndex;
    private final ImmutableMap<QueryParam, Integer> resultParamsByIndex;

    @Override
    @Nonnull
    public String getQuery() {
        return query;
    }

    @Override
    @Nonnull
    public List<Integer> getQueryIndices(@Nonnull QueryParam param) throws InvalidKeyException {
        return QueryInfoProcessing.getQueryIndices(param, queryParamsByIndex, this.name());
    }

    @Override
    public int getResultIndex(@Nonnull QueryParam param) throws InvalidKeyException {
        return QueryInfoProcessing.getResultIndex(param, resultParamsByIndex, this.name());
    }

    MerchantQuery(@Nonnull final String query,
                  @Nonnull final ImmutableMap<QueryParam, ImmutableList<Integer>> queryParamsByIndex,
                  @Nonnull final ImmutableMap<QueryParam, Integer> resultParamsByIndex) {
        this.query = query;
        this.queryParamsByIndex = queryParamsByIndex;
        this.resultParamsByIndex = resultParamsByIndex;
    }

}
