package com.expenditures.db;

import com.expenditures.categories.ExpenditureCategory;

import javax.annotation.Nonnull;
import java.sql.*;
import java.util.Collection;

public class DatabaseConnections {

    private DatabaseConnections(){}

    @Nonnull
    private static Connection getExpendituresConnection(final boolean shouldUseAutoCommit) throws SQLException {
        //TODO - move this String to Config
        try {
            Class.forName("org.postgresql.Driver");
        }
        catch (final ClassNotFoundException e) {
            throw new RuntimeException("Unable to load Postgres driver", e);
        }
        final Connection connection =  DriverManager.getConnection("jdbc:postgresql://localhost/finances");
        connection.setAutoCommit(shouldUseAutoCommit);
        return connection;
    }

    @Nonnull
    public static Connection getExpendituresConnection() throws SQLException {
       return getExpendituresConnection(true);
    }

    @Nonnull
    public static Connection getNonAutoCommitExpendituresConnection() throws SQLException {
        return getExpendituresConnection(false);
    }

    @Nonnull
    public static Array convertExpendituresToSqlArray(@Nonnull final Connection connection,
                                                      @Nonnull final Collection<ExpenditureCategory> expenditureCategories) throws SQLException {
        //TODO - move this String to Config?s
        return connection.createArrayOf(JDBCType.INTEGER.getName(), expenditureCategories.stream().map(ExpenditureCategory::getId).toArray());
    }

    @Nonnull
    public static Array convertIntListToSqlArray(@Nonnull final Connection connection,
                                                 @Nonnull final Collection<Integer> collection) throws SQLException {
        return connection.createArrayOf(JDBCType.INTEGER.getName(), collection.toArray());
    }

    @Nonnull
    public static Array convertStringListToSqlArray(@Nonnull final Connection connection,
                                                   @Nonnull final Collection<String> collection) throws SQLException {
        return connection.createArrayOf(JDBCType.VARCHAR.getName(), collection.toArray());
    }
}
