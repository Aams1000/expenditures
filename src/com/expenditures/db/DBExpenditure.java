package com.expenditures.db;

import javax.annotation.Nonnull;
import java.time.LocalDate;

public class DBExpenditure {

    private final long id;
    private final LocalDate transactionDate;
    private final String merchant;
    private final double amount;
    private final int categoryId;

    public DBExpenditure(final long id,
                         @Nonnull final LocalDate transactionDate,
                         @Nonnull final String merchant,
                         final double amount,
                         final int categoryId) {
        this.id = id;
        this.transactionDate = transactionDate;
        this.merchant = merchant;
        this.amount = amount;
        this.categoryId = categoryId;
    }

    public long getId() {
        return id;
    }

    @Nonnull
    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    @Nonnull
    public String getMerchant() {
        return merchant;
    }

    public double getAmount() {
        return amount;
    }

    public int getCategoryId() {
        return categoryId;
    }
}