package com.expenditures.db;

import javax.annotation.Nonnull;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public class PreparedStatements {

    private PreparedStatements(){}

    public static void setDates(@Nonnull final PreparedStatement preparedStatement,
                                @Nonnull final List<Integer> indices,
                                @Nonnull final LocalDate date) throws SQLException {
        for (final int index : indices) {
            preparedStatement.setDate(index, java.sql.Date.valueOf(date));
        }
    }

    public static void setStrings(@Nonnull final PreparedStatement preparedStatement,
                                 @Nonnull final List<Integer> indices,
                                 @Nonnull final String value) throws SQLException {
        for (final int index : indices) {
            preparedStatement.setString(index, value);
        }
    }

    public static void setInts(@Nonnull final PreparedStatement preparedStatement,
                               @Nonnull final List<Integer> indices,
                               final int value) throws SQLException {
        for (final int index : indices) {
            preparedStatement.setInt(index, value);
        }
    }

    public static void setDoubles(@Nonnull final PreparedStatement preparedStatement,
                                  @Nonnull final List<Integer> indices,
                                  final double value) throws SQLException {
        for (final int index : indices) {
            preparedStatement.setDouble(index, value);
        }
    }

    public static void setArrays(@Nonnull final PreparedStatement preparedStatement,
                                 @Nonnull final List<Integer> indices,
                                 final Array value) throws SQLException {
        for (final int index : indices) {
            preparedStatement.setArray(index, value);
        }
    }

    public static void setLongs(@Nonnull final PreparedStatement preparedStatement,
                                 @Nonnull final List<Integer> indices,
                                 final long value) throws SQLException {
        for (final int index : indices) {
            preparedStatement.setLong(index, value);
        }
    }
}
