package com.expenditures.db;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.util.List;

/**
 * This feels pretty hacky, as it's creating a bizarre inheritance behavior. I can't add this method to the QueryInfo
 * interface, and I don't like how it's copy/pasted in each implementing enum.
 */
public class QueryInfoProcessing {

    private QueryInfoProcessing(){}

    @Nonnull
    public static List<Integer> getQueryIndices(@Nonnull QueryParam param,
                                         @Nonnull final ImmutableMap<QueryParam, ImmutableList<Integer>> queryParamsByIndex,
                                         @Nonnull final String name) throws InvalidKeyException {
        final List<Integer> indices = queryParamsByIndex.get(param);
        if (indices == null) {
            throw new InvalidKeyException("Query " + name + " does not support the parameter " + param.name());
        }
        return indices;
    }

    public static int getResultIndex(@Nonnull QueryParam param,
                                     @Nonnull final ImmutableMap<QueryParam, Integer> resultParamsByIndex,
                                     @Nonnull final String name) throws InvalidKeyException {
        final Integer index = resultParamsByIndex.get(param);
        if (index == null) {
            throw new InvalidKeyException("Query result " + name + " does not support the parameter " + param.name());
        }
        return index;
    }
}
