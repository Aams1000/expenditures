package com.expenditures.db;

public enum QueryParam {

    START_DATE,
    END_DATE,
    CATEGORY_ID,
    MONTH,
    AMOUNT,
    MERCHANT,
    DATE_LAST_MODIFIED,
    DATE_OF_TRANSACTION,
    FILE_NAME,
    EXPENDITURE_ID,
    CATEGORY_NAME,
    FILE_ID,
    ID_TO_DELETE,
    REPLACEMENT_ID,
    IS_CUSTOM,
    ACCOUNT_TYPE,
    MERCHANT_ARRAY,
    COUNT
}
