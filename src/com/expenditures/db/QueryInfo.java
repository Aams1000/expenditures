package com.expenditures.db;

import javax.annotation.Nonnull;
import java.security.InvalidKeyException;
import java.util.List;

public interface QueryInfo {

    @Nonnull
    String getQuery();

    @Nonnull
    List<Integer> getQueryIndices(@Nonnull QueryParam param) throws InvalidKeyException;

    int getResultIndex(@Nonnull QueryParam param) throws InvalidKeyException;
}
